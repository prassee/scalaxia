organization := "scalaxia"

name := "scalaxia"

version := "1.0"

scalaVersion := "2.9.1"

seq(webSettings :_*)

seq(Twirl.settings: _*)

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % "2.0.3",
  "org.scalatra" %% "scalatra-specs2" % "2.0.3" % "test",
  "org.eclipse.jetty" % "jetty-webapp" % "7.5.4.v20111024" % "container",
  "javax.servlet" % "servlet-api" % "2.5" % "provided",
  "org.scalatest" %% "scalatest" % "1.7.1" % "test"
)

resolvers += "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"