package scalaxia

import org.scalatest.fixture.FunSuite
import org.scalatest.junit.AssertionsForJUnit
import org.junit.Test
import scalaxia.connector.TwitterDelegate
import scalaxia.manager.ScalaxiaManager
import scalaxia.cache.ScalaxiaCacheManager

class TwitterConnectorSuite extends AssertionsForJUnit {

	val tc = TwitterDelegate()

	@Test
	def checkTwitterIsCreated {
		assert(tc != null)
	}

	@Test
	def obtainTweets {
		try {
			val tweets = tc.collectPaginatedTweets(1)
			assert(!tweets.isEmpty)
			assert(tweets.size > 0)
		} catch {
			case (e: Exception) => {
				println(e.printStackTrace)
				fail("failed to get tweets")
			}
		}
	}
}

class ScalaxiaManagerTest extends AssertionsForJUnit {
	val manager = new ScalaxiaManager

	@Test
	def testTweets() {
		val tweets = manager.getScalaxiaTweets
		assert(!tweets.isEmpty)
		assert(tweets.size > 0)
	}

	@Test
	def testScalaxiaFriends() {
		val friends = manager.getScalaxiaFriends
		assert(!friends.isEmpty)
		assert(friends.size > 0)
	}
}

class ScalaxiaCacheTest extends AssertionsForJUnit {
	val cache = ScalaxiaCacheManager
	val cd = cache.getTweets

	@Test
	def testScalaxiaCache() {
		assert(cd.size > 0)
		val cd1 = cache.getTweets
		assert(cd.size == cd1.size)
	}

}