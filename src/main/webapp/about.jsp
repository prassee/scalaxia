<jsp:include page="header.jsp"/>        
        <!-- left content -->
        <div class="span12">
          <div class="row">
			       <h1>About Scalaxia <small>what's Scalaxia BTW ?</small></h1>	                                                   
          </div> 
          <div class="row">
			       <div class="span12">             
                
                
Welcome to Scalaxia!!! the galaxy of Scala tweets
</p>
<p>
This project is inspired from the Groovy community website groovytweets.org.
</p>

<p>
The idea is to unleash the power of twitter to make hub for the Scala related tweets around the planet.
</p>

<p>
<b>How it works?</b><br>
<blockquote style="line-height:120%">
This uses Twitter API to scroll out the tweets and using a basic pattern matching to extract Scala related technology tweets from the friends of <a href="http://twitter.com/#!/scalaxia">@scalaxia</a> twitter handler.
</blockquote>
<br>
This application list of all the Friends tweets which has Scala relate keywords.
</p>

<p>
<b>How to make my tweets to be added ?</b><br>
<blockquote style="line-height:120%">
All you need to do is using your twitter id follow our <a href="http://twitter.com/#!/scalaxia">@scalaxia</a> handler. With in some days your account will be refollowed. Once this happens then any of your twitter having any scala related text will be added automatically into scalaxia website.
</blockquote>
</p>

<p>
<b>How long my tweet will be displayed?</b><br>
<blockquote style="line-height:120%">
We currently listing max of 30 current tweets displayed in our site.
</blockquote>
</p>
              
             </div>
          </div>
       </div>        
<jsp:include page="sidebar.jsp"/>
<jsp:include page="footer.jsp"/>
