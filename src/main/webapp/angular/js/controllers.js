function UserController($scope,$http) {	
	$scope.phones = $http.jsonp("/users").success(function(data) {
    	$scope.phones= data;
  	});
	
	$scope.createUser = function() {
		if($scope.user.pwd==$scope.user.rpwd) {
			$http({
	    	method: 'POST',
	    	url: '/createUser',
	    	data: 'name=' + $scope.user.name + '&email=' +$scope.user.email +'&pwd='+$scope.user.pwd,
	    	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			})
		} else {
			$scope.pwdDonMatch ="<div class=\"alert alert-error\">Password Don Match</div>"
		}	
	}
}