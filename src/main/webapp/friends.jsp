<%@page import="java.util.Collection"%>
<%@page import="scalaxia.dto.Friend"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1" %>

<jsp:include page="header.jsp"/>        
        <!-- left content -->
        <div class="span12">
        <div class="row">
			       <h1>Friends<small>&nbsp; here's our friends who about scala ... </small></h1>	                                                   
          </div>
       <% for(Friend friend:(Collection<Friend>)request.getAttribute("friends")) { %>	
          <div class="row">
            
            <div class="span1">
              <center>
                <img src="<%=friend.getProfImgUrl()%>" width="48" height="48" alt="profile Image">
              </center>
            </div>
                         
            <div class="span10 block-message info" style="font-size:18px;line-height:120%"> 
             <b><%=friend.getScreenName() %></b>&nbsp;- &nbsp; <b> <small><%=friend.getLocation()%></small> </b>
             <p>   
             <span><%out.println(friend.getDescription()); %></span>
             </p>                                
            </div>                                       
          </div> 
		<%}%>
       </div>        
<jsp:include page="sidebar.jsp"/>
<jsp:include page="footer.jsp"/>
