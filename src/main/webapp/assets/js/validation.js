function validateRegistration() {
	$("#register").submit(function(e) {
		if (e.target.name.value === "") {
			alert("Please Enter your name");
			$(e.target.name).focus();
			return false;
		}
		var validEmail = new RegExp("^[a-zA-Z0-9]+\@[a-zA-Z0-9]+\.[a-zA-Z0-9]{2,}").test(e.target.email.value)
		if (!validEmail) {
			alert("Please enter a valid Email address");
			$(e.target.email).focus();
			return false;
		} else if(validEmail) {
			/* script for ajax call to find if the valid email 
			 already registered for the event and throw a alert msg*/
		}
		if (e.target.knowabout.value === "") {
			alert("How do you know about this event ?");
			$(e.target.knowabout).focus();
			return false;
		}
		return true;
	});
}


function validateCreateEvent() {
	$("#create").submit(function(e) {
		if (e.target.name.value === "") {
			alert("Please Enter the Name of the event");
			$(e.target.name).focus();
			return false;
		}
		if (e.target.desc.value === "") {
			alert("give some details about the event");
			$(e.target.desc).focus();
			return false;
		}
		return true;
	});
}
