<jsp:include page="header.jsp"/>        
        <!-- left content -->
        <div class="span12">
          <div class="row">
			<h1>Team
			<small>People behind Project Scalaxia</small></h1>
          </div> 

          <div class="row">			
			<div class="span1">
				<img src="http://a2.twimg.com/profile_images/1242772158/Untitled-1_normal.jpg">
			</div>
			<div class="span10 block-message info">
				<strong>Founder & Co-Creator</strong>
				<strong>Rajmahendra ( Raj )</strong>
				Java developer, voracious reader, JUG Chennai Founder & Lead <a href="http://twitter.com/#!/jug_c">@jug_c</a></br>
Follow me <a href="http://twitter.com/#!/rajonjava">@rajonjava</a><br>
				
			</div>
          </div>

          <div class="row">			
			<div class="span1">
				<img src="http://a0.twimg.com/profile_images/1155106349/6d9ad137-dbf5-467b-93ba-ef72a991fe6b_normal.png">
			</div>
			<div class="span10 block-message info">
				<strong>Creator & Co-Founder</strong>
				<strong>Prasanna</strong>
				A Java programmer, now attracted by Scala. Iam a regular speaker at <a href="http://www.jugchennai.in">JUG Chennai</a> and a committer of <a href="http://code.google.com/p/kandash/">Kandash</a> project.
				<br>
				Follow me <a href="http://twitter.com/#!/prasonscala">@prasonscala</a><br>
				I blog at <a href="http://prasonscala.tumblr.com">tumblr</a>
				Primary Member <a href="http://jug-c.wikispaces.com/CHEnnai+Scala+Enthusiast">CHESE</a>
			</div>
          </div>
          
          <div class="row">			
			<div class="span1">
				<img src="https://si0.twimg.com/profile_images/1525187900/Profile.pic.jpg" width="48" height="48">
			</div>
			<div class="span10 block-message info">
				<strong>Interface Designer</strong>
				<strong>Gautham</strong>
				Eternal Linux Fan , jQuery Coder, Regular JUG Chennai Member.<br>
				I enjoy coding especially in Java , jQuery/JavaScript and Python.<br>
				<br>
				FollowMe <a href="http://twitter.com/#!/gautamNitish">@gautamNitish</a><br>
				I blog at : http://j.mp/ab2pdox<br> 
				GitHub: https://github.com/gautamk<br>
			</div>
          </div>
          
          <div class="row">			
			<div class="span1">
				<img src="https://twimg0-a.akamaihd.net/profile_images/971152290/fb_bigger.jpg" width="48" height="48">
			</div>
			<div class="span10 block-message info">
				<strong>Android Programmer</strong>
				<strong>Ragunath Jawahar</strong>
				Android enthusiast, entrepreneur and avid programmer.<br>
				FollowMe <a href="http://twitter.com/#!/ragunathjawahar">@ragunathjawahar</a><br>
			</div>
          </div>
          
       </div>   
            
<jsp:include page="sidebar.jsp"/>
<jsp:include page="footer.jsp"/>
