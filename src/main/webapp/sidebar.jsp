 <!-- right content -->
   <div class="span4">
              
       <div class="row">
          <div class="span4 block-message success">
          <div class="span1">                
              <img src="http://a0.twimg.com/profile_images/1517223860/scalaxia128.png" width="48" height="48">
          </div> 

          <div class="span3">
            <h1>Scalaxia</h1>
          </div>
          
          <center>
            <h3>BETA</h3>
          </center>
          </div>
       </div>
		
		<!--
		<div class="row">
			<center>
          	 <a href="/signin"> <img src="/Sign-in-with-Twitter-darker.png" alt="sign to twitter"> </a>          
          	</center> 
        </div>
       -->




 <div class="row">    
<div class="span4">
                <div class="block-message alert-message warning" >
                    Throw us a
                    <a href="https://twitter.com/share"
                       class="twitter-share-button"
                       data-count="horizontal"
                       data-via="scalaxia"
                       data-related="#scala">Tweet</a>
                </div>
                <div  class="block-message alert-message warning" >
                    <div>
                        If your want your Scala related tweets to be displayed in this site please
                        <a href="https://twitter.com/scalaxia" class="twitter-follow-button">Follow @scalaxia</a>
                    </div>
                </div>
                <div class="block-message alert-message warning" >
                    <iframe src="//www.facebook.com/plugins/likebox.php?
                    href=http%3A%2F%2Fwww.facebook.com%2Fscalaxia&amp;
                    width=190&amp;
                    height=290&amp;
                    colorscheme=light&amp;
                    show_faces=true&amp;
                    border_color=transparent&amp;
                    stream=false&amp;
                    header=false"
                            scrolling="no"
                            frameborder="0"
                            style="
                                border:none;
                                overflow:hidden;
                                width:190px;
                                height:290px;"
                            allowTransparency="true">
                    </iframe>
                </div>
 </div>
 </div>



<!--       
       <div class="row">                                 
          <div class="span4">
          	<%
          	String lscrName = (String)session.getAttribute("loggedInUser");
          	if(lscrName != null) {
          		out.println(lscrName);
          	}
          	%>
            If your want your Scala related tweets to be displayed in this site please 
            follow <a href="http://twitter.com/#!/scalaxia">@scalaxia</a>.
          </div>
      </div>  

 

      <div class="row">
          <div class="span4">
            <h3>Important Links</h3>
            <hr>
          </div>
          <div class="span4">
            <ul>
              <li>
                <a href="http://scala-lang.org">Scala</a>
              </li>
              <li>
                <a href="https://github.com/scalatra/scalatra">Scalatra</a>
              </li>
              <li>
                <a href="http://scalate.fusesource.org">Scalate</a>
              </li>
              <li>
                <a href="http://liftweb.net/">Lift</a>
              </li>
            </ul>
          </div>
       </div>
--> 


<!--
 	<div class="row">
          <div class="span4">
            <h3>Scala Books</h3>
            <hr>
          </div>
          <div class="span4">
	<a href="http://www.scala-lang.org/node/959" target="_blank">
	<img src="" id="SlideImage"/></a>
	
	<script>
		var image_list = [''];
		var current_img_no=0;
		var rotate_time_out = 10 * 1000 ; // In milli seconds
		var DATA;
		var effect ='fade';
		var options = {};
		var speed = 6 * 1000;
		var album_url="https://picasaweb.google.com/data/feed/base/user/103145713941623982840/albumid/5646887469890449521?alt=json&kind=photo&hl=en_GB";
		//album_url = album_url + "&jsoncallback=?";
		$.getJSON(album_url, function(data){
			DATA = data;
			image_list.shift();
			$.each(data.feed.entry,function(i,item){
				var temp = new Image();
				temp.src = item.content.src;
				image_list.push(temp);
			})
			NextImage();
			setInterval('NextImage();',rotate_time_out);	
		});
		
		function NextImage(){
			if(current_img_no == (image_list.length - 1))
				current_img_no = -1;
			
			current_img_no++;
			
			$('#SlideImage').hide(effect , options , speed , function(){
				$('#SlideImage').attr('src',image_list[current_img_no].src).show(effect , options , speed);
			} );
		}
	</script>
 <br/>
  <i>courtesy <a href="http://www.scala-lang.org">scala-lang.org</a></i>
          </div>
       </div>
       
-->


		<!-- supported browsers-->
 <div class="row">

          <div class="span4">

            <h3>Best viewed with</h3>
            <hr>
          </div>
          <div class="span4">
            <ul>
            <li>Latest <a href="http://www.google.com/chrome" target="_blank">Chrome</a></li>
             <li>Latest <a href="http://www.apple.com/safari/" target="_blank">Safari</a></li>
            <li>Latest <a href="http://firefox.com" target="_blank">Firefox</a></li>
            <li>Latest <a href="http://www.opera.com/" target="_blank">Opera</a></li>
            <li>or latest IE</li>
            </ul>
	</div>
 
       </div>
       
       <div class="row">
        <div class="span4">
          <h3>Powered by</h3>
          <hr>
        </div>
        <div class="span4">
                <a href="http://scala-lang.org/">
                 <img src="http://a1.twimg.com/profile_images/1049043677/scala_logo_bigger_bigger.png" width="40" alt="scala">
                </a>
                <br>
                <a href="http://twitter4j.org/">
                  <img src="http://twitter4j.org/en/images/twitter4j-logo.png" width="140" alt="twitter4j">
                </a>
                <br>
                <a href="http://code.google.com/appengine/">
                  <img src="http://code.google.com/appengine/images/appengine-noborder-120x30.gif" 
                    alt="Powered by Google App Engine" />
                </a>
                <br>
                <a href="http://twitter.github.com/bootstrap/">Bootstrap, from Twitter</a>
                <br>
                and obviously
                <br>
                <a href="http://twitter.com/"><img src="http://a0.twimg.com/profile_images/1124040897/at-twitter.png"></a>
        </div>
       </div>
    </div> 


 
