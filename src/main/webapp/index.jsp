<%@page import="java.util.Collection"%>
<%@page import="scalaxia.dto.HomeLineTweet"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1" %>

<jsp:include page="header.jsp"/>        
        <!-- left content -->
        <div class="span12">
        	<script>
	          	function showEnglish(txt) {
	          		google.load("language", "1");
	          		google.language.transliterate($("#"+txt).html(),"fr","en",callback);
	          	}
	          	
	          	var callback = function (result) {
	          		alert(result.translation);
	          	}
            </script>
        
        <% int i = 0;
        for(HomeLineTweet status:(Collection<HomeLineTweet>)request.getAttribute("tweets")) { %>	
          <div class="row">
            <div class="span1">
              <center>
                <img src="<%=status.getProfImgUrl()%>" width="48" height="48" alt="profile Image">
              </center>
            </div>             
            <div class="span10 block-message info" style="font-size:18px;line-height:120%"> 
             <b><%=status.getScreenName() %></b>&nbsp;- &nbsp;                                   
             <span id="scalaxia<%=i%>"><%out.println(status.getText()); %></span>
              <div class="row offset5">
                <div class="span5" style="text-align:right">
                  <p><strong>
                	<small>
                  	<a href="https://twitter.com/intent/tweet?in_reply_to=<%=status.getTweetId()%>">Reply</a> &nbsp;
                  	<a href="https://twitter.com/intent/retweet?tweet_id=<%=status.getTweetId()%>">Retweet</a> &nbsp;
                  	<a href="https://twitter.com/intent/favorite?tweet_id=<%=status.getTweetId()%>">Favorite</a>
                  	</small>
                  	<small style="color:black"><%=status.getCreatedAt()%></small>
                  	</strong></p>
                </div>
              </div>  
            </div>                                       
          </div> 
		<%i++;
		}%>
       </div>        
<jsp:include page="sidebar.jsp"/>
<jsp:include page="footer.jsp"/>
