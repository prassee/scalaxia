<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Scalaxia</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="/favicon.ico">
	<link rel="stylesheet" href="bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
	<script src="https://www.google.com/jsapi?key=ABQIAAAAMt9YZQaG_wbfmb2826e_wBTPUDO5TVHJS-HZdrhJpqnB5yZcfRQFObTd1-bPphEIY11228Z78VhA6A"></script>
	<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27354677-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </head>

  <body>
    
    <section id="navigation">

        <div class="page-header"> 
        <h1>Navigation</h1>     
        </div> 
        <div class="topbar-wrapper" style="z-index: 5;"> 
        <div class="topbar"> 
        <div class="fill"> 
        <div class="container"> 
        
        <h3><a href="/">Scalaxia</a></h3>
        
        <ul>         
        <li><img src="http://a0.twimg.com/profile_images/1517223860/scalaxia128.png" width="32" height="32"></li>
        <!-- <li><a href="/contributors">Participants</a></li>  --> 
        <li><a href="/team">Team</a></li> 
        <li><a href="/friends">Friends</a></li> 
        <li><a href="/about">About</a></li>
        </ul>
   
        </div> 
        </div> <!-- /fill --> 
        </div> <!-- /topbar --> 
        </div> <!-- topbar-wrapper --> 
    <section>
    <div class="container">
     <div class="row">
