/*
* Copyright 2011 Prasanna & Rajmahendra
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*
*	Project Owners
*	==============
*	Prasanna Kumar @prasanna
*	Rajmahendra Hegde @rajmahendra
* */
package scalaxia.servlet

import javax.servlet.http.HttpServlet
import scalaxia.cache.ScalaxiaCacheManager
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * servlet to sync the cache
 * @author prasanna
 */
class CacheSyncServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {
		ScalaxiaCacheManager.updateCache(ScalaxiaCacheManager.STATUS_KEY)
	}
}

/**
 * servlet to sync the friends cache
 * @author prasanna
 */
class SyncFriendsServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {
		ScalaxiaCacheManager.updateCache(ScalaxiaCacheManager.FRIENDS_KEY)
	}
}