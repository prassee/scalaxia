/*
* Copyright 2011 Prasanna & Rajmahendra
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*
*	Project Owners
*	==============
*	Prasanna Kumar @prasanna
*	Rajmahendra Hegde @rajmahendra
* */
package scalaxia.servlet

import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.FilterConfig
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import scalaxia.cache.ScalaxiaCacheManager
import scalaxia.connector.TwitterDelegate
import twitter4j.Twitter
import twitter4j.auth.RequestToken

/**
 * the home page of the website points to this servlet
 * @author prasanna
 */
class ScalaxiaServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {
		resp.setContentType("text/html")
		req.setAttribute("tweets", ScalaxiaCacheManager.getTweets())
		req.getRequestDispatcher("index.jsp").forward(req, resp)
		setUserDetails(req)
	}

	private def setUserDetails(req: HttpServletRequest) {
		val isLoggedIn = Option(req.getSession().getAttribute("twitter"))
		if (isLoggedIn.isDefined) {
			val screenName = isLoggedIn.get.asInstanceOf[Twitter].getScreenName()
			req.getSession().setAttribute("loggedInUser", screenName)
		}
	}
}

/**
 * @author prasanna
 */
class TwitterLoginServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {
		val twit = TwitterDelegate.getWebTwitter
		req.getSession().setAttribute("twitter", twit)

		val buffer: StringBuffer = req.getRequestURL()
		buffer.replace(buffer.lastIndexOf("/"), buffer.length(), "").append("/callback")

		val requestToken = twit.getOAuthRequestToken(buffer.toString())
		req.getSession().setAttribute("requestToken", requestToken)
		resp.sendRedirect(requestToken.getAuthenticationURL())
	}
}

/**
 * @author prasanna
 */
class TwitterCallBackServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
		val twitter = req.getSession().getAttribute("twitter").asInstanceOf[Twitter]
		val reqtoken = req.getSession().getAttribute("requestToken").asInstanceOf[RequestToken]
		val verifier = req.getParameter("oauth_verifier")
		twitter.getOAuthAccessToken(reqtoken, verifier)
		req.getSession().removeAttribute("requestToken")
		resp.sendRedirect(req.getContextPath() + "/")
	}
}

/**
 * the router to show the desired page
 * the routes could be used while migrating the play
 * framework
 * @author prasanna
 */
class ScalaxiaFilter extends Filter {
	override def doFilter(req: ServletRequest,
		resp: ServletResponse, filterChain: FilterChain) = {
		req.asInstanceOf[HttpServletRequest].getRequestURI() match {
			
			// case "/" => req.getRequestDispatcher("/scalaxia").forward(req, resp)
			case "/json" => req.getRequestDispatcher("/scalaxiaJson").forward(req, resp)
			case "/scalaxia.json" => req.getRequestDispatcher("/scalaxiaJson").forward(req, resp)
			case "/scalaxiaFriends.json" => req.getRequestDispatcher("/scalaxiaFriendsJson").forward(req, resp)

			case "/team" => req.getRequestDispatcher("team.jsp").forward(req, resp)
			case "/about" => req.getRequestDispatcher("about.jsp").forward(req, resp)
			case "/friends" => {
				req.setAttribute("friends", ScalaxiaCacheManager.getFriends)
				req.getRequestDispatcher("friends.jsp").forward(req, resp)
			}

			case "/syncCache" => ScalaxiaCacheManager.updateCache(ScalaxiaCacheManager.STATUS_KEY)
			case "/syncFriends" => ScalaxiaCacheManager.updateCache(ScalaxiaCacheManager.FRIENDS_KEY)

			case _ => filterChain.doFilter(req, resp)
		}
	}
	override def init(config: FilterConfig) {}
	override def destroy() = {}
}
