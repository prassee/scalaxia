/*
* Copyright 2011 Prasanna & Rajmahendra
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*
*	Project Owners
*	==============
*	Prasanna Kumar @prassee
*	Rajmahendra Hegde @rajmahendra
* */
package scalaxia.servlet

import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import scalaxia.cache.ScalaxiaCacheManager._
import scalaxia.implicits.jsonConverter._

/**
 * *
 * @author prassee
 */
class ScalaxiaJSONServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {
		resp.setContentType("application/json")
		// resp.getWriter().println(convertToJSON(getTweets))
	}
}

/**
 * *
 * @author prassee
 */
class ScalaxiaFriendsJSONServlet extends HttpServlet {
	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {
		resp.setContentType("application/json")
		// resp.getWriter().println(convertToJSON(getFriends))
	}
}