package scalaxia.views

import org.scalatra.ScalatraServlet
import scalaxia.cache.ScalaxiaCacheManager._
import scalaxia.dto.HomeLineTweet
import scalaxia.dto.Friend
import org.scalatra.ScalatraFilter
import scalaxia.implicits.jsonConverter._
import com.google.gson.Gson

/*
 * 
 */
class WebView extends ScalatraServlet with TwirlSupport {

	get("/") {
		val twets = getTweets.asInstanceOf[List[HomeLineTweet]]
		views.html.index.render(tweets = twets)
	}

	get("/friends") {
		val frnds = getFriends.asInstanceOf[List[Friend]]
		views.html.friends.render(friends = frnds)
	}
	
	get("/scalaxia.json") {
		contentType = "application/json"
		convertToJSON(getTweets)
	}
	
	get("/scalaxiaFriends.json") {
		contentType = "application/json"
		convertToJSON(getFriends)
	}
}
