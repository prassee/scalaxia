/* Copyright 2011 Prasanna & Rajmahendra
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*
*	Project Owners
*	==============
*	Prasanna Kumar @prasanna
*	Rajmahendra Hegde @rajmahendra
*/
package scalaxia.cache

import java.util.HashMap
import java.util.logging.Logger
import scala.collection.JavaConversions
import com.google.gson.Gson
import scalaxia.dto.Friend
import scalaxia.dto.HomeLineTweet
import scalaxia.manager.ScalaxiaManager
import scalaxia.dto.Cachable
import scala.collection.JavaConversions._
import scalaxia.dto.HomeLineTweet
import scalaxia.dto.HomeLineTweet
import scalaxia.dto.HomeLineTweet

/*
 * 
 * This is a temporary form of a cache.
 *
 * The reason behind this cache is,
 * application cannot afford to connect to twitter
 * each time to display the tweets therefore on bootstrap
 * the first hit to display tweets will
 * create an entry in the map and subse	quent invocations
 * are served from the map rather the connecting to twitter.
 *
 * However this map will be refreshed by a cron job from GAE
 * environment.
 *
 * This cache has to be enhanced by means of a original cache API.
 *
 * @author prasanna
 */
object ScalaxiaCacheManager {
	
	val STATUS_KEY = "statuses"
	val FRIENDS_KEY = "friends"

	private val cache = new HashMap[String, List[_ <: Cachable]]()
	private val logger = Logger.getLogger(this.getClass().getName())
	private val manager = new ScalaxiaManager
	
	def getTweets() = getDataFromCache[HomeLineTweet](STATUS_KEY)
	def getFriends() = getDataFromCache[Friend](FRIENDS_KEY)

	def updateCache(key: String) {
		logger.info("obtaining data to be cached")
		key match {
			case STATUS_KEY => updateCacheValues(
				manager.getScalaxiaTweets(), STATUS_KEY)
			case FRIENDS_KEY => updateCacheValues(
				manager.getScalaxiaFriends, FRIENDS_KEY)
		}
	}

	private def updateCacheValues(list: List[_ <: Cachable], key: String) {
		if (list.size > 0) {
			cache.remove(key)
			logger.info("updating cache")
			cache.put(key, list)
			logger.info("cache updated !!!! ")
		} else {
			logger.info("no new data found retaining the existing cache")
		}
	}

	private def getDataFromCache[T](key: String) = {
		if (!Option(cache.get(key)).isDefined) {
			updateCache(key)
		}
		logger.info("obtaining data from cache")
		cache.get(key).reverse
	}

}