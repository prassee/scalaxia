package scalaxia.manager

import scalaxia.connector.TwitterDelegate
import scalaxia.dto.HomeLineTweet
import twitter4j.Status
import scalaxia.implicits.StatusConverter._
import scalaxia.implicits.ResponseListConverter._
import scalaxia.dto.Friend

class ScalaxiaManager {
	
	private val twitterBridge = TwitterDelegate()
	
	// better consider a dictionary for this
	private def keywords = Array("scala", "scalatra", "akka", "scaladays", "scalate",
		"lift", "liftweb", "sbt-idea", "sbt-netbeans-plugin", "salat", "asyncfp",
		"Bowler", "scalaide", "Scamacs", "sbt", "maven-scala-plugin", "sbteclipse",
		"lifty", "sbt-idea", "Scalariform", "ScalaConsole", "Scalide", "ENSIME",
		"ScalaCheck", "ScalaTest", "Specs2", "Specs", "Qualac", "sindi",
		"scalaz", "scalabazaar", "Scuff", "Logula", "scala-guice", "scalamodules",
		"sjson", "Jerkson", "scalaxb", "Scales", "scala-arm", "typesafe",
		"scala-io", "scalajpa", "ScalaQuery", "Squeryl", "scala-migrations",
		"spray", "Sweet", "BlueEyes", "Unfiltered", "Pinky", "giter8", "scalathon",
		"scalaxia", "play2.0", "playframework", "hammersmith", "Scalatron", "slick")

	/**
	 * the gateway method that delivers the tweets to the clients
	 */
	def getScalaxiaTweets(): List[HomeLineTweet] = {
		var statuses = List[HomeLineTweet]()
		for (status <- collectTweetsBasedOnPagination.toList.sorted.reverse) {
			statuses = statuses.::(status)
		}
		statuses
	}

	/**
	 * function to ensure that we have credible no of tweets to display
	 * in the page.
	 */
	private def collectTweetsBasedOnPagination: Set[Status] = {
		val filter = new ScalaxiaKeywordFilter(
				twitterBridge.collectPaginatedTweets(1, 3),keywords)
		val finalOutput = {
			var filteredTweets = Set[Status]()
			for (a <- keywords) yield {
				filteredTweets = filteredTweets.++(
					filter.filterTweets(filter.tweetFilterFn(a)))
			}
			filteredTweets
		}
		finalOutput
	}
	
	def getScalaxiaFriends(): List[Friend] = {
		var users = List[Friend]()
		val screenName = twitterBridge.getHandlerName
		sliceFriendsIds(twitterBridge.getFriendsIds(),0, 75, users)
	}

	private def sliceFriendsIds(array: Array[Long], midpoint: Int, incre: Int,
		values: List[Friend]): List[Friend] = {
		var tempValues = values
		if (midpoint < array.length) {
			val sliced = array.slice(midpoint, midpoint + incre)
			for (usr <- twitterBridge.lookupUsers(sliced)) {
				tempValues = tempValues.::(usr)
			}
			sliceFriendsIds(array, midpoint + incre, incre, tempValues)
		} else {
			tempValues
		}
	}
	
}

class ScalaxiaKeywordFilter(statusSet: Set[Status], keywords: Array[String]) {

	def filterTweets(filterFn: Status => Boolean) = {
		statusSet.filter(filterFn)
	}

	def tweetFilterFn(keyword: String)(status: Status) = {
		status.getText.toLowerCase().contains(keyword)
	}
}