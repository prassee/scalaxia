/*
* Copyright 2011 Prasanna & Rajmahendra
*
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License.
*
*	Project Owners
*	==============
*	Prasanna Kumar @prassee
*	Rajmahendra Hegde @rajmahendra
* */
package scalaxia.dto

/**
 * a trait for all cachable objects
 */
trait Cachable {}

/**
 * @author prasanna
 * DTO class to hold the values of the status object
 */
class HomeLineTweet(profImgUrl: String, screenName: String,
	text: String, plainText: String,
	createdAt: String, tweetId: String,
	isRetweeted: Boolean = false,
	needsTranslation: Boolean = false) extends Cachable {
	def getProfImgUrl = profImgUrl
	def getScreenName = screenName
	def getText = text
	def getPlainText = plainText
	def getCreatedAt = createdAt
	def getTweetId = tweetId
	def getIsRetweeted = isRetweeted
	def getNeedsTranslation = needsTranslation
}

/**
 * @author prasanna
 * DTO class to hold the values of the scalaxia friends object
 */
class Friend(profImgUrl: String, screenName: String,
	location: String, desc: String) extends Cachable {
	def getProfImgUrl = profImgUrl
	def getScreenName = screenName
	def getLocation = location
	def getDescription = desc
}