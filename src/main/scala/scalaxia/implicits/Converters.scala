/*
 * Copyright 2011 Prasanna & Rajmahendra
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 *	Project Owners
 *	==============
 *	Prasanna Kumar @prassee
 *	Rajmahendra Hegde @rajmahendra
 * */
package scalaxia.implicits

import org.apache.commons.lang3.StringEscapeUtils._
import org.knallgrau.utils.textcat.TextCategorizer
import com.twitter.Autolink
import scalaxia.dto.HomeLineTweet
import twitter4j.ResponseList
import twitter4j.Status
import twitter4j.util.TimeSpanConverter
import twitter4j.User
import scalaxia.dto.Friend
import java.util.Collection
import scalaxia.dto.Cachable
import com.google.gson.Gson
import scala.collection.JavaConversions

object StatusConverter {
	val autolink = new Autolink()
	val converter = new TimeSpanConverter()
	val guesser = new TextCategorizer()

	implicit def convertStatusToHomeLineTweet(status: Status) = {
		val statusTxt = status.getText()
		val screenName = status.getUser().getScreenName()
		new HomeLineTweet(status.getUser().getProfileImageURL().toString(),
			screenName,
			autolink.autoLink(statusTxt),
			escapeHtml4(statusTxt).replaceAll("#", "%23"),
			converter.toTimeSpanString(status.getCreatedAt()), status.getId().toString(),
			status.isRetweetedByMe(),
			(!guesser.categorize(statusTxt).equals("english") &&
				isUserReqTransltn(screenName)))
	}

	private def isUserReqTransltn(screenName: String) =
		Array("xuwei_k", "scalaby").filter(_.equals(screenName)).length > 0

}

object ResponseListConverter {
	implicit def convertResponseListToBuffer[T](responseList: ResponseList[T]) = {
		scala.collection.JavaConversions.asScalaBuffer(responseList.subList(0, responseList.size))
	}

	implicit def convertUserToFriend(usr: User) = {
		new Friend(usr.getProfileImageURL().toString(),
			usr.getScreenName(), usr.getLocation(), usr.getDescription())
	}
}

object jsonConverter {
	val gson = new Gson
	def convertToJSON(collection:List[_ <: Cachable]) = {
		gson.toJson(JavaConversions.asJavaList(collection))
	}
}