package scalaxia.connector

import twitter4j.TwitterFactory
import twitter4j.conf.ConfigurationBuilder
import twitter4j.Paging
import twitter4j.Status
import twitter4j.Twitter
import twitter4j.CursorSupport

class TwitterDelegate(twitter: Twitter) {

	/**
	 * function to ensure that we have enough no of tweets to display
	 * in the page.
	 */
	def collectPaginatedTweets(pageindex: Int,
		hopCount: Int = 3):	 Set[Status] = {
		var lis = getTweetsToProcess(pageindex)
		for (i <- pageindex to hopCount) {
			lis = lis ++ getTweetsToProcess(i)
		}
		lis
	}

	def getHandlerName() = twitter.getScreenName

	def getFriendsIds() = twitter.getFriendsIDs(getHandlerName, CursorSupport.START).getIDs

	def lookupUsers(userIds: Array[Long]) = twitter.lookupUsers(userIds)

	private def getTweetsToProcess(page: Int = 1, tweets: Int = 75): Set[Status] = {
		var tweetsToFilter = Set[Status]()
		var paging = new Paging(page, tweets)
		var homeLineStatuss = twitter.getHomeTimeline(paging)
		for (i <- 0 until (homeLineStatuss.size())) {
			if (!homeLineStatuss.get(i).isRetweet())
				tweetsToFilter = tweetsToFilter.+(homeLineStatuss.get(i))
		}
		tweetsToFilter
	}
}

/**
 * The class which talks to twitter with the help of twitter's API credentials
 * and helps in obtaining tweets under applications desired criteria
 * @author prasanna
 */
object TwitterDelegate {

	private val cb = new ConfigurationBuilder
	private val twitter = new TwitterFactory(cb.build()).getInstance()
	def getTwitter = twitter
	def apply() = new TwitterDelegate(getTwitter)

	/**	
	 * method to serve twitter object for user logged in over web
	 */
	def getWebTwitter = {
		val cb = new ConfigurationBuilder
		cb.setOAuthConsumerKey("XdlFsP6cQIPbxRfG1Daaw")
		cb.setOAuthConsumerSecret("HbK3nmmRHGKb7v1zW1I0ShHMrH47OVKUCYSwRqwLFA")
		new TwitterFactory(cb.build()).getInstance()
	}
}